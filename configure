#!/usr/bin/env bash

# Default configuration
version="unknown"
prefix="/usr/local"
winepath="/opt/wine-compholio/bin/wine"
mozpluginpath="/usr/lib/mozilla/plugins"
gccruntimedlls=""
win32cxx=""
win32flags=""
quietinstallation="true"
nogpuaccel="false"

usage ()
{
	echo ""
	echo "Usage: ./configure [--prefix=PREFIX] [--wine-path=PATH]"
	echo "                   [--moz-plugin-path=PATH] [--gcc-runtime-dlls=PATH]"
	echo "                   [--win32-prebuilt] [--win32-cxx=COMPILER]"
	echo "                   [--win32-static] [--win32-flags=FLAGS]"
	echo "                   [--show-installation-dialogs] [--no-gpu-accel]"
	echo ""
}

while [[ $# > 0 ]] ; do
	CMD="$1"; shift
	case "$CMD" in
		--prefix=*)
			prefix=${CMD#*=}
			;;
		--prefix)
			prefix="$1"; shift
			;;

		--wine-path=*)
			winepath=${CMD#*=}
			;;
		--wine-path)
			winepath="$1"; shift
			;;

		--moz-plugin-path=*)
			mozpluginpath=${CMD#*=}
			;;
		--moz-plugin-path)
			mozpluginpath="$1"; shift
			;;

		--gcc-runtime-dlls=*)
			gccruntimedlls=${CMD#*=}
			;;
		--gcc-runtime-dlls)
			gccruntimedlls="$1"; shift
			;;

		--show-installation-dialogs)
			quietinstallation="false"
			;;

		--win32-static)
			win32flags="$win32flags -static-libgcc -static-libstdc++ -static"
			;;
		--win32-prebuilt)
			win32cxx="prebuilt"
			;;

		--win32-cxx=*)
			win32cxx=${CMD#*=}
			;;
		--win32-cxx)
			win32cxx="$1"; shift
			;;

		--win32-flags=*)
			win32flags="$win32flags "${CMD#*=}
			;;
		--win32-flags)
			win32flags="$win32flags $1"; shift
			;;

		--no-gpu-accel)
			nogpuaccel="true"
			;;

		--help)
			usage
			exit
			;;
		*)
			echo "WARNING: Unknown argument $CMD." >&2
			;;
	esac
done

# Try to autodetect compiler
if [ -z "$win32cxx" ]; then
	if command -v i686-w64-mingw32-g++ >/dev/null 2>&1; then
		win32cxx="i686-w64-mingw32-g++"
		win32flags="$win32flags -static-libgcc -static-libstdc++ -static"

	elif command -v i686-pc-mingw32-g++ > /dev/null 2>&1; then
		win32cxx="i686-pc-mingw32-g++"
		win32flags="$win32flags -DMINGW32_FALLBACK -static-libgcc -static-libstdc++ -static"

	elif command -v i586-mingw32msvc-c++ > /dev/null 2>&1; then
		win32cxx="i586-mingw32msvc-c++"
		win32flags="$win32flags -DMINGW32_FALLBACK -static-libgcc -static-libstdc++ -static"

	else
		echo "ERROR: No mingw32-g++ compiler found. Please use --win32cxx to specify one."
		exit 1
	fi
fi

# Get the version number
changelog=$(head -n1 debian/changelog)
if [[ "$changelog" =~ \((.*)\)\ (UNRELEASED)? ]]; then
	version="${BASH_REMATCH[1]}"
	if [ "${BASH_REMATCH[2]}" == "UNRELEASED" ]; then
		version="$version-daily"
	fi
fi

# Normalize the paths
prefix=$(readlink -m "$prefix")
winepath=$(readlink -m "$winepath")
mozpluginpath=$(readlink -m "$mozpluginpath")
gccruntimedlls=$(readlink -m "$gccruntimedlls")

echo "Configuration Summary"
echo "---------------------"
echo ""
echo "Pipelight has been configured with"
echo " version           = '$version'"
echo " prefix            = '$prefix'"
echo " winepath          = '$winepath'"
echo " mozpluginpath     = '$mozpluginpath'"
echo " gccruntimedlls    = '$gccruntimedlls'"
echo " quietinstallation = '$quietinstallation'"
echo " win32cxx          = '$win32cxx'"
echo " win32flags        = '$win32flags'"
echo " nogpuaccel        = '$nogpuaccel'"
echo ""
echo "IMPORTANT: Please ensure you have XATTR support enabled for both wine and"
echo "           your file system (required to watch DRM protected content)!"
echo ""

echo "version=$version"						>  config.make
echo "prefix=$prefix" 						>> config.make
echo "winepath=$winepath"					>> config.make
echo "mozpluginpath=$mozpluginpath"			>> config.make
echo "gccruntimedlls=$gccruntimedlls"		>> config.make
echo "quietinstallation=$quietinstallation" >> config.make
echo "win32cxx=$win32cxx"					>> config.make
echo "win32flags=$win32flags" 				>> config.make
echo "nogpuaccel=$nogpuaccel"				>> config.make

exit 0